/*
Copyright (C) 1996-1997 Id Software, Inc.
 
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
 
See the GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

#include "qwsvdef.h"

#ifndef _WIN32
#include <fcntl.h>
#endif

#define MAX_ACCOUNTS 1000
#define MAX_FAILURES 10
#define MAX_LOGINNAME (DIGEST_SIZE * 2 + 1)
#define SESSION_TIME 60*60

cvar_t sv_login = {"sv_login", "0"};	// if enabled, login required
cvar_t sv_login_accounts_file_path = {"sv_login_accounts_file_path", "accounts.dat"};
cvar_t sv_login_sessions_file_path = {"sv_login_sessions_file_path", "sessions.dat"};

extern cvar_t sv_hashpasswords;

typedef enum {a_free, a_ok, a_blocked} acc_state_t;
typedef enum {use_log, use_ip} quse_t;

typedef struct
{
    char	login[MAX_LOGINNAME];
    char	pass[MAX_LOGINNAME];
    int         failures;
    int         inuse;
    ipfilter_t	adress;
    acc_state_t	state;
    quse_t	use;
    qbool       muted;
} account_t;
static account_t accounts[MAX_ACCOUNTS];
static int       num_accounts = 0;

typedef struct {
    char    cookie[MAX_LOGINNAME];
    char    login[MAX_LOGINNAME];
    byte    ip[4];
    double  expiration;
} session_t;
static session_t sessions[MAX_ACCOUNTS];
static int       num_sessions;

#ifndef _WIN32

static FILE *open_and_lock_sessions_file(void)
{
    FILE *f = fopen(va("%s/%s", fs_gamedir, sv_login_sessions_file_path.string), "rb+");
    struct flock lock = {
        F_WRLCK,
        SEEK_SET,
        0,
        0,
        getpid()
    };
  
    if (!f) {
        Sys_Printf("Failed to open sessions file...\n");
        return NULL;
    }

    if (fcntl(fileno(f), F_SETLKW, &lock) != -1)
        return f;

    fclose(f);
    return NULL;
}

static void read_sessions_file(FILE *f)
{
    if (feof(f)) {
        return;
    }

    fread(&num_sessions, 1, sizeof(int), f);
    fread(sessions, num_sessions, sizeof(session_t), f);
}

static void write_sessions_file(FILE *f)
{
    fseek(f, 0, SEEK_SET);
    fwrite(&num_sessions, 1, sizeof(int), f);
    fwrite(sessions, num_sessions, sizeof(session_t), f);
}

static FILE *open_sessions_for_writing(void)
{
    FILE      *f;

    f = open_and_lock_sessions_file();
    if (!f)
        return NULL;

    read_sessions_file(f);

    return f;
}

static void remove_sessions(const char *login)
{
    session_t   *s;
    int         i;
    FILE        *f;
    int         c;

    f = open_sessions_for_writing();
    if (!f)
        return;

    fwrite(&c, 1, sizeof(int), f);
    for (i = 0, c = 0; i < num_sessions; ++i) {
        s = &sessions[i];

        if (!login && s->expiration < realtime) {
            Sys_Printf("Expired session [%s] for %s\n", s->cookie, s->login);
            continue;
        }
        if (login && !strncmp(login, s->login, MAX_LOGINNAME)) {
            Sys_Printf("Removing session [%s] for %s\n", s->cookie, s->login);
            continue;
        }

        fwrite(s, 1, sizeof(session_t), f);
        c++;
    }
    fseek(f, 0, SEEK_SET);
    fwrite(&c, 1, sizeof(int), f);

    fclose(f);
}

static session_t *create_session(client_t *cl)
{
    session_t *s;
    account_t *a; // for adding the password to the hash
    char      tmp[MAX_LOGINNAME];
    FILE      *f;

    // remove previous sessions for this user
    remove_sessions(cl->login);

    f = open_sessions_for_writing();
    if (!f)
        return NULL;

    if (num_sessions == MAX_ACCOUNTS)
        return NULL;

    a = accounts + cl->logged - 1;
    sprintf(tmp, "%i%i", rand() % 999999, cl->logged - 1);
    strlcat(tmp, a->login, MAX_LOGINNAME);
    strlcat(tmp, a->pass, MAX_LOGINNAME);

    s = &sessions[num_sessions];
    s->expiration = realtime + SESSION_TIME;
    /* *(int *)s->ip = *(int *)cl->realip.ip; */
    *(int *)s->ip = *(int *)cl->netchan.remote_address.ip;
    strlcpy(s->login, cl->login, MAX_LOGINNAME);
    strlcpy(s->cookie, SHA1(tmp), MAX_LOGINNAME);
    num_sessions++;

    write_sessions_file(f);
    fclose(f);

    MSG_WriteByte (&cl->netchan.message, svc_stufftext);
    MSG_WriteString (&cl->netchan.message, va("setinfo cookie %s\n", s->cookie));

    Sys_Printf("New session [%s] for %s\n", s->cookie, s->login);

    return s;
}

static session_t *find_session(byte ip[], char *cookie)
{
    int i;
    for (i = 0; i < num_sessions; ++i) {
        session_t *s = &sessions[i];
        if (*(int *)ip == *(int *)s->ip && !strncmp(cookie, s->cookie, MAX_LOGINNAME)) {
            return s;
        }
    }
    return NULL;
}

static void renew_session(byte ip[], char *cookie)
{
    FILE      *f;
    session_t *s;

    f = open_sessions_for_writing();
    if (!f)
        return;

    s = find_session(ip, cookie);
    if (!s)
        return;
    s->expiration = realtime + SESSION_TIME;

    write_sessions_file(f);
    fclose(f);

    return;
}

static void load_sessions(void)
{
    FILE *f = open_and_lock_sessions_file();
    if (!f)
        return;
    read_sessions_file(f);
    fclose(f);
}
#endif

static account_t *find_account(char *login)
{
    int i;
    for (i = 0; i < num_accounts; ++i) {
        account_t *a = &accounts[i];
        if (a->state == a_free)
            continue;
        if (!strncmp(a->login, login, MAX_LOGINNAME))
            return a;
    }
    return NULL;
}

#ifndef _WIN32
static qbool login_by_session(client_t *cl)
{
    char        *client_cookie;
    session_t   *s;

    client_cookie = Info_Get(&cl->_userinfo_ctx_, "cookie");
    if (!client_cookie || !client_cookie[0]) {
        Sys_Printf("EMPTY COOKIE\n");
        return false;
    }

    load_sessions();
    s = find_session(cl->netchan.remote_address.ip, client_cookie);
    if (!s) {
        Sys_Printf("SESSION NOT FOUND %s\n", client_cookie);
        return false;
    }

    if (s->expiration > realtime) {
        account_t *a;

        a = find_account(s->login);
        if (!a)
            return false;

        a->inuse++;
        cl->logged = a - accounts + 1;
        strlcpy(cl->login, s->login, MAX_LOGINNAME);
        cl->muted = a->muted;

        renew_session(s->ip, s->cookie);

        return true;
    }
    remove_sessions(NULL);
    return false;
}

static void clear_sessions_file()
{
    FILE *f = fopen(va("%s/%s", fs_gamedir, sv_login_sessions_file_path.string), "wb");
    struct flock lock = {
        F_WRLCK,
        SEEK_SET,
        0,
        0,
        getpid()
    };

    if (!f) {
        Sys_Printf("Failed to open sessions file...\n");
        return;
    }

    fcntl(fileno(f), F_SETLKW, &lock);
    num_sessions = 0;
    write_sessions_file(f);
    fclose(f);
}
#endif

static qbool validAcc(char *acc)
{
    char *s = acc;

    for (; *acc; acc++)
    {
        if (*acc < 'a' || *acc > 'z')
            if (*acc < 'A' || *acc > 'Z')
                if (*acc < '0' || *acc > '9')
                    return false;
    }

    return acc - s <= MAX_LOGINNAME && acc - s >= 3 ;
}

/*
=================
WriteAccounts
 
Writes account list to disk
=================
*/

static void WriteAccounts()
{
    int c;
    FILE *f;
    account_t *acc;

    //Sys_mkdir(ACC_DIR);
    if ((f = fopen(va("%s/%s", fs_gamedir, sv_login_accounts_file_path.string), "wt")) == NULL) {
        Con_Printf("Warning: couldn't open accounts file for writing.\n");
        return;
    }

    for (acc = accounts, c = 0; c < num_accounts; acc++)
    {
        if (acc->state == a_free)
            continue;

        if (acc->use == use_log)
            fprintf(f, "%s %s %d %d %d\n", acc->login, acc->pass, acc->state, acc->failures, (int)acc->muted);
        else
            fprintf(f, "%s %s %d %d\n", acc->login, acc->pass, acc->state, (int)acc->muted);

        c++;
    }

    fclose(f);

    // force cache rebuild.
    FS_FlushFSHash();
}

/*
=================
SV_LoadAccounts
 
loads account list from disk
=================
*/
qbool StringToFilter (char *s, ipfilter_t *f);
void SV_LoadAccounts(void)
{
    int i;
    FILE *f;
    account_t *acc = accounts;
    client_t *cl;

    if ((f = fopen(va("%s/%s", fs_gamedir, sv_login_accounts_file_path.string), "rt")) == NULL) {
        Con_DPrintf("couldn't open accounts file\n");
        // logout
        num_accounts = 0;
        for (cl = svs.clients; cl - svs.clients < MAX_CLIENTS; cl++)
        {
            if (cl->logged > 0)
                cl->logged = 0;
            cl->login[0] = 0;
        }
        return;
    }

    while (!feof(f))
    {
        memset(acc, 0, sizeof(account_t));
        // Is realy safe to use 'fscanf(f, "%s", s)'? FIXME!
        if (fscanf(f, "%s", acc->login) != 1) {
            Con_Printf("Error reading account data\n");
            break;
        }
        if (StringToFilter(acc->login, &acc->adress))
        {
            strlcpy(acc->pass, acc->login, MAX_LOGINNAME);
            acc->use = use_ip;
            if (fscanf(f, "%s %d %d\n", acc->pass, (int *)&acc->state, (int *)&acc->muted) != 3) {
                Con_Printf("Error reading account data\n");
                break;
            }
        }
        else {
            if (fscanf(f, "%s %d %d %d\n", acc->pass,  (int *)&acc->state, &acc->failures, (int *)&acc->muted) != 4) {
                Con_Printf("Error reading account data\n");
                break;
            }
        }

        if (acc->state != a_free) // lol?
            acc++;
    }

    num_accounts = acc - accounts;

    fclose(f);

    // for every connected client check if their login is still valid
    for (cl = svs.clients; cl - svs.clients < MAX_CLIENTS; cl++) {
        if (cl->state < cs_connected || cl->logged <= 0)
            continue;

        for (i = 0, acc = accounts; i < num_accounts; i++, acc++) {
            if ((acc->use == use_log && !strncmp(acc->login, cl->login, CLIENT_LOGIN_LEN)) ||
                (acc->use == use_ip  && !strcmp(acc->login, va("%d.%d.%d.%d", cl->realip.ip[0], cl->realip.ip[1], cl->realip.ip[2], cl->realip.ip[3])))) {
                break;
            }
        }

        if (i < num_accounts && acc->state == a_ok) {
            // login again if possible
            if (!acc->inuse || acc->use == use_ip) {
                cl->logged = i+1;
                if (acc->use == use_ip)
                    strlcpy(cl->login, acc->pass, CLIENT_LOGIN_LEN);

                acc->inuse++;
                continue;
            }
        }

        // login is not valid anymore, logout
        cl->login[CLIENT_LOGIN_LEN-1] = 0;
        Sys_Printf("Invalid login:\n\tcl->login = '%s'\n\tcl->logged = '%i'\n\ti = '%i'\n\tacc->state = '%i'\n", cl->login, cl->logged, i, acc->state);

        cl->logged = 0;
        cl->login[0] = 0;
    }
}



/*
=================
SV_CreateAccount_f
 
acc_create <login> [<password>]
if password is not given, login will be used for password
login/pass has to be max 16 chars and at least 3, only regular chars are acceptable
=================
*/

void SV_CreateAccount_f(void)
{
    int i, spot, c;
    ipfilter_t adr;
    quse_t use;

    if (Cmd_Argc() < 2)
    {
        Con_Printf("usage: acc_create <login> [<password>]\n       acc_create <address> <username>\nmaximum %d characters for login/pass\n", MAX_LOGINNAME - 1); //bliP: adress typo
        return;
    }

    SV_LoadAccounts(); // Prevent loss of synchronism between servers

    if (num_accounts == MAX_ACCOUNTS)
    {
        Con_Printf("MAX_ACCOUNTS reached\n");
        return;
    }

    if (StringToFilter(Cmd_Argv(1), &adr))
    {
        use = use_ip;
        if (Cmd_Argc() < 3)
        {
            Con_Printf("usage: acc_create <address> <username>\nmaximum %d characters for username\n", MAX_LOGINNAME - 1); //bliP; adress typo
            return;
        }
    }
    else
    {
        use = use_log;

        // validate user login/pass
        if (!validAcc(Cmd_Argv(1)))
        {
            Con_Printf("Invalid login!\n");
            return;
        }

        if (Cmd_Argc() == 4 && !validAcc(Cmd_Argv(2)))
        {
            Con_Printf("Invalid pass!\n");
            return;
        }
    }

    // find free spot, check if login exist;
    for (i = 0, c = 0, spot = -1 ; c < num_accounts; i++)
    {
        if (accounts[i].state == a_free)
        {
            if (spot == -1)	spot = i;
            continue;
        }

        if (!strcasecmp(accounts[i].login, Cmd_Argv(1)) ||
            (use == use_ip && !strcasecmp(accounts[i].login, Cmd_Argv(2))))
            break;

        c++;
    }

    if (c < num_accounts)
    {
        Con_Printf("Login already in use\n");
        return;
    }

    if (spot == -1)
        spot = i;

    // create an account
    num_accounts++;
    strlcpy(accounts[spot].login, Cmd_Argv(1), MAX_LOGINNAME);
    if (Cmd_Argc() == 3)
        i = 2;
    else
        i = 1;
    strlcpy(accounts[spot].pass, (int)sv_hashpasswords.value && use == use_log ?
            SHA1(Cmd_Argv(i)) : Cmd_Argv(i), MAX_LOGINNAME);

    accounts[spot].state = a_ok;
    accounts[spot].use = use;

    Con_Printf("login %s created\n", Cmd_Argv(1));
    WriteAccounts();
}

/*
=================
SV_RemoveAccount_f
 
acc_remove <login>
removes the login
=================
*/

void SV_RemoveAccount_f(void)
{
    int i, c;

    if (Cmd_Argc() < 2)
    {
        Con_Printf("usage: acc_remove <login>\n");
        return;
    }

    SV_LoadAccounts(); // Prevent loss of synchronism between servers

    for (i = 0, c = 0; c < num_accounts; i++)
    {
        if (accounts[i].state == a_free)
            continue;

        if (!strcasecmp(accounts[i].login, Cmd_Argv(1)))
        {
            if (accounts[i].inuse)
                SV_Logout(&svs.clients[accounts[i].inuse -1]);

            accounts[i].state = a_free;
            num_accounts--;
            Con_Printf("login %s removed\n", accounts[i].login);
            WriteAccounts();
            return;
        }
        c++;
    }
    Con_Printf("account for %s not found\n", Cmd_Argv(1));
}

/*
=================
SV_ListAccount_f

shows the list of accounts
=================
*/

void SV_ListAccount_f (void)
{
    int i,c;

    SV_LoadAccounts(); // Prevent loss of synchronism between servers

    if (!num_accounts)
    {
        Con_Printf("account list is empty\n");
        return;
    }

    Con_Printf("account list:\n");

    for (i = 0, c = 0; c < num_accounts; i++)
    {
        if (accounts[i].state != a_free)
        {
            Con_Printf("%.16s %s\n", accounts[i].login, accounts[i].state == a_ok ? "" : "blocked");
            c++;
        }
    }

    Con_Printf("%d login(s) found\n", num_accounts);
}

/*
=================
SV_blockAccount

blocks/unblocks an account
=================
*/

void SV_blockAccount(qbool block)
{
    int i, c;
    SV_LoadAccounts(); // Prevent loss of synchronism between servers
    for (i = 0, c = 0; c < num_accounts; i++)
    {
        if (accounts[i].state == a_free)
            continue;

        if (!strcasecmp(accounts[i].login, Cmd_Argv(1)))
        {
            if (block)
            {
                accounts[i].state = a_blocked;
                Con_Printf("account %s blocked\n", Cmd_Argv(1));
                return;
            }

            if (accounts[i].state != a_blocked)
                Con_Printf("account %s not blocked\n", Cmd_Argv(1));
            else
            {
                accounts[i].state = a_ok;
                accounts[i].failures = 0;
                Con_Printf("account %s unblocked\n", Cmd_Argv(1));
            }

            return;
        }
        c++;
    }

    Con_Printf("account %s not found\n", Cmd_Argv(1));
}

void SV_UnblockAccount_f(void)
{
    if (Cmd_Argc() < 2)
    {
        Con_Printf("usage: acc_unblock <login>\n");
        return;
    }
    SV_LoadAccounts(); // Prevent loss of synchronism between servers
    SV_blockAccount(false);
    WriteAccounts();
}

void SV_BlockAccount_f(void)
{
    if (Cmd_Argc() < 2)
    {
        Con_Printf("usage: acc_block <login>\n");
        return;
    }
    SV_LoadAccounts(); // Prevent loss of synchronism between servers
    SV_blockAccount(true);
    WriteAccounts();
}

static client_t *find_client(const char *login) {
    client_t *cl;
    for (cl = svs.clients; cl - svs.clients < MAX_CLIENTS; cl++) {
        if (!strncmp(cl->login, login, MAX_LOGINNAME))
            return cl;
    }
    return NULL;
}

void SV_MuteAccount_f(void)
{
    int i;

    if (Cmd_Argc() < 2) {
        Con_Printf("usage: acc_mute <login>\n");
        return;
    }

    SV_LoadAccounts();
    for (i = 0; i < num_accounts; i++) {
        account_t *a = &accounts[i];
        if (a->state == a_free)
            continue;

        if (!strcasecmp(a->login, Cmd_Argv(1))) {
            client_t *cl = find_client(a->login);

            if (a->muted) {
                a->muted = false;
                Con_Printf("account %s unmuted\n", Cmd_Argv(1));
            } else {
                a->muted = true;
                Con_Printf("account %s muted\n", Cmd_Argv(1));
            }
            if (cl)
                cl->muted = a->muted;
        }
    }
    WriteAccounts();
}

/*
=================
checklogin

returns positive value if login/pass are valid
values <= 0 indicates a failure
=================
*/
static int checklogin(char *log1, char *pass, client_t *cl, quse_t use)
{
    int i,c;

    for (i = 0, c = 0; c < num_accounts; i++) {
        if (accounts[i].state == a_free)
            continue;

        if (use == accounts[i].use && !strcasecmp(log1, accounts[i].login)) {
            if (accounts[i].inuse && accounts[i].use == use_log)
                return -1;

            if (accounts[i].state == a_blocked)
                return -2;

            if (use == use_ip ||
                (!(int)sv_hashpasswords.value && !strcasecmp(pass,       accounts[i].pass)) ||
                ( (int)sv_hashpasswords.value && !strcasecmp(SHA1(pass), accounts[i].pass)))
            {
                accounts[i].failures = 0;
                accounts[i].inuse++;
                cl->muted = accounts[i].muted;
                return i+1;
            }
            return 0;
        }
        c++;
    }
    return 0;
}

void Login_Init (void)
{
    Cvar_Register(&sv_login);
    Cvar_Register(&sv_login_accounts_file_path);
    Cvar_Register(&sv_login_sessions_file_path);
    Cmd_AddCommand ("acc_create",SV_CreateAccount_f);
    Cmd_AddCommand ("acc_remove",SV_RemoveAccount_f);
    Cmd_AddCommand ("acc_list",SV_ListAccount_f);
    Cmd_AddCommand ("acc_unblock",SV_UnblockAccount_f);
    Cmd_AddCommand ("acc_block",SV_BlockAccount_f);
    Cmd_AddCommand ("acc_mute", SV_MuteAccount_f);

#ifndef _WIN32
    clear_sessions_file();
#endif
}

/*
===============
SV_Login
 
called on connect after cmd new is issued
===============
*/

qbool SV_Login(client_t *cl)
{
    extern cvar_t sv_registrationinfo;
    char info[256];
    char *ip;

    // is sv_login is disabled OR client is VIP, login is not necessery
    if (!(int)sv_login.value || cl->vip)
    {
        SV_Logout(cl);
        cl->logged = -1;
        return true;
    }

    // if we're already logged return (probobly map change)
    if (cl->logged > 0)
        return true;

    // sv_login == 1 -> spectators don't login
    if ((int)sv_login.value == 1 && cl->spectator)
    {
        SV_Logout(cl);
        cl->logged = -1;
        return true;
    }

#ifndef _WIN32
    // login by session
    if (login_by_session(cl)) {
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, va("Your session was restored, and it's valid for %i mins.\n", SESSION_TIME/60));

        return true;
    }
#endif

    // check for account for ip
    ip = va("%d.%d.%d.%d", cl->realip.ip[0], cl->realip.ip[1], cl->realip.ip[2], cl->realip.ip[3]);
    if ((cl->logged = checklogin(ip, ip, cl, use_ip)) > 0)
    {
        strlcpy(cl->login, accounts[cl->logged-1].pass, CLIENT_LOGIN_LEN);
        return true;
    }

    // need to login before connecting
    cl->logged = false;
    cl->login[0] = 0;

    if (sv_registrationinfo.string[0])
    {
        strlcpy (info, sv_registrationinfo.string, 253);
        strlcat (info, "\n\n", 255);
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, info);
    }
    MSG_WriteByte (&cl->netchan.message, svc_print);
    MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
    MSG_WriteString (&cl->netchan.message, "Enter your login and password:\n");

    return false;
}

void SV_Logout(client_t *cl)
{
    if (cl->logged > 0)
    {
        accounts[cl->logged-1].inuse--;
        cl->login[0] = 0;
        cl->logged = 0;
    }
}

qbool SV_LoginCheckSay(client_t *cl)
{
    account_t *acc;
    char *text;
    char *ws;
    int cmp;
	
    acc = find_account(cl->login);
    if (!acc)
        return true;

    text = Cmd_Argv(1);

    ws = strstr(text, " ");
    if (ws)
        *ws = 0;
    cmp = strncmp(SHA1(text), acc->pass, MAX_LOGINNAME);
    if (ws)
        *ws = ' ';
	
    if (cmp)
        return true;
	
    SV_ClientPrintf(cl, PRINT_HIGH, "You almost leaked your password!\n");
    return false;
}

void SV_ParseLogin(client_t *cl)
{
    extern cvar_t sv_forcenick;
    char *log1, *pass;

    if (Cmd_Argc() > 2)
    {
        log1 = Cmd_Argv(1);
        pass = Cmd_Argv(2);
    }
    else
    { // bah usually whole text in 'say' is put into ""
        log1 = pass = Cmd_Argv(1);
        while (*pass && *pass != ' ')
            pass++;

        if (*pass)
            *pass++ = 0;

        while (*pass == ' ')
            pass++;
    }

    // if login is parsed, we read just a password
    if (cl->login[0])
    {
        pass = log1;
        log1 = cl->login;

        // Password retrying throttle
        if (realtime - cl->lastloginattempt < 5) {
            MSG_WriteByte (&cl->netchan.message, svc_print);
            MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
            MSG_WriteString (&cl->netchan.message, va("Wait %i sec(s)\n", 5-(int)(realtime - cl->lastloginattempt)));
            return;
        }
        cl->lastloginattempt = realtime;
    }
    else
    {
        strlcpy(cl->login, log1, CLIENT_LOGIN_LEN);
    }

    if (!*pass)
    {
        strlcpy(cl->login, log1, CLIENT_LOGIN_LEN);
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, va("Password for %s:\n", cl->login));

        return;
    }

    cl->logged = checklogin(log1, pass, cl, use_log);

    switch (cl->logged)
    {
    case -2:
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, "Login blocked\nEnter your login and password:\n");
        cl->logged = 0;
        cl->login[0] = 0;
        break;
    case -1:
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, "Login in use!\ntry again:\n");
        cl->logged = 0;
        cl->login[0] = 0;
        break;
    case 0:
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, va("Access denied\nPassword for %s:\n", cl->login));
        break;
    default:
        Sys_Printf("%s logged in as %s\n", cl->name, cl->login);
#ifndef _WIN32
        if (create_session(cl)) {
            MSG_WriteByte (&cl->netchan.message, svc_print);
            MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
            MSG_WriteString (&cl->netchan.message, va("Welcome %s! Your login is valid for %i mins.\n", log1, SESSION_TIME/60));
        } else {
            MSG_WriteByte (&cl->netchan.message, svc_print);
            MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
            MSG_WriteString (&cl->netchan.message, va("Welcome %s!\n", log1));
        }
#else
        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, va("Welcome %s!\n", log1));
#endif

        //VVD: forcenick ->
        if (((int)sv_forcenick.value || cl->muted) && cl->login) {
            Info_Set (&cl->_userinfo_ctx_, "name", cl->login);
            strlcpy (cl->name, cl->login, CLIENT_NAME_LEN);
            MSG_WriteByte (&cl->netchan.message, svc_stufftext);
            MSG_WriteString (&cl->netchan.message, va("name %s\n", cl->login));
            MSG_WriteByte (&cl->netchan.message, svc_stufftext);
            MSG_WriteString (&cl->netchan.message, va("setinfo name %s\n", cl->login));
        }
        //<-

        MSG_WriteByte (&cl->netchan.message, svc_stufftext);
        MSG_WriteString (&cl->netchan.message, "cmd new\n");
    }
}

void SV_LoginCheckTimeOut(client_t *cl)
{
    if (cl->connection_started && realtime - cl->connection_started > 25)
    {
        Sys_Printf("login time out for %s\n", cl->name);

        MSG_WriteByte (&cl->netchan.message, svc_print);
        MSG_WriteByte (&cl->netchan.message, PRINT_HIGH);
        MSG_WriteString (&cl->netchan.message, "Waited too long, Bye!\n");
        SV_DropClient(cl);
    }
}

void Cmd_ChangePassword_f(void) {
    int i, c;

    // Check if the player is currently logged in
    if (!(sv_client->logged > 0)) {
        SV_ClientPrintf(sv_client, PRINT_HIGH, "You must be logged in to change your password.\n");
        return;
    }

    // Check command parameters
    if (Cmd_Argc() < 4) {
        SV_ClientPrintf(sv_client, PRINT_HIGH, "usage: changepass <current password> <new password> <new password confirmation>\nmaximum %d characters for the password\n", MAX_LOGINNAME-1);
        return;
    }

    // Check the validity of the arguments
    for (i = 1; i < Cmd_Argc(); ++i) {
        if (!validAcc(Cmd_Argv(i))) {
            SV_ClientPrintf(sv_client, PRINT_HIGH, "Invalid password specified at argument %i.\nmaximum %d characters for the password\n", i, MAX_LOGINNAME-1);
            return;
        }
    }

    // Check if the new password and confirmation match
    if (strncmp(Cmd_Argv(2), Cmd_Argv(3), MAX_LOGINNAME)) {
        SV_ClientPrintf (sv_client, PRINT_HIGH, "The new password and the confirmation doesn't match. Password hasn't been changed.\n");
        return;
    }

    SV_LoadAccounts(); // Prevent loss of synchronism between servers
    // Find the account and change the password, if the current one matches
    for (i = 0, c = 0; c < num_accounts; i++) {
        if (accounts[i].state == a_free)
            continue;
        if (!strcasecmp(accounts[i].login, sv_client->login)) {
            if ((!(int)sv_hashpasswords.value && !strcasecmp(Cmd_Argv(1),       accounts[i].pass)) ||
                ( (int)sv_hashpasswords.value && !strcasecmp(SHA1(Cmd_Argv(1)), accounts[i].pass))) {
                strlcpy(accounts[i].pass, (int)sv_hashpasswords.value ? SHA1(Cmd_Argv(2)) : Cmd_Argv(2), MAX_LOGINNAME);
                WriteAccounts();
                SV_ClientPrintf (sv_client, PRINT_HIGH, "Your password has been changed successfully!\n");
                return;
            }
            SV_ClientPrintf (sv_client, PRINT_HIGH, "The current password specified isn't valid. Password hasn't been changed.\n");
            return;
        }
        c++;
    }
    return;
}

void Cmd_ListLoggedUsers_f(void) {
    client_t *cl;

    for (cl = svs.clients; cl - svs.clients < MAX_CLIENTS; cl++) {
        if (cl->state < cs_connected)
            continue;
        SV_ClientPrintf(sv_client, PRINT_HIGH, "%s --> %s%s\n", cl->name, cl->logged > 0 ? cl->login : "Not logged in", cl->muted ? " [muted]" : "");
    }
}

void SV_Login_StuffAliases(client_t *cl) {
    MSG_WriteByte (&cl->netchan.message, svc_stufftext);
    MSG_WriteString (&cl->netchan.message, va("alias listusers \"cmd listusers\"\nalias changepass \"cmd changepass %0\"\n"));
}
